pub fn square_of_sum(n: u32) -> u32 {
    (1..n + 1)
        .try_fold(0, |acc: u32, x| acc.checked_add(x))
        .unwrap_or_default()
        .pow(2)
}

pub fn sum_of_squares(n: u32) -> u32 {
    (1..n + 1)
        .map(|x| x.pow(2))
        .try_fold(0, |acc: u32, x| acc.checked_add(x))
        .unwrap_or_default()
}

pub fn difference(n: u32) -> u32 {
    square_of_sum(n) - sum_of_squares(n)
}
