pub fn nth(n: usize) -> u32 {
    let mut primes = vec![2];
    let mut next = 3;
    while &primes.len() <= &n {
        let mut next_is_prime = true;
        for p in &primes {
            if next % p == 0 {
                next_is_prime = false;
                break;
            }
        }
        if next_is_prime {
            primes.push(next);
        }
        next = next + 1;
    }
    primes.pop().unwrap()
}
