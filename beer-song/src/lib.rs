pub fn verse(n: i32) -> String {
    let no_or_number = |n: i32| match n {
        0 => "no more".to_string(),
        n => n.to_string(),
    };

    let bottle_or_bottles = |n: i32| match n {
        1 => "bottle",
        _ => "bottles",
    };

    let store_or_drink = match n {
        0 => "Go to the store and buy some more",
        1 => "Take it down and pass it around",
        _ => "Take one down and pass it around",
    };

    let (now, after) = (no_or_number(n), no_or_number(n - 1));
    let (bottle_now, bottle_after) = (bottle_or_bottles(n), bottle_or_bottles(n - 1));
    let left = match n {
        0 => 99.to_string(),
        _ => after,
    };

    format! { "{} {} of beer on the wall, {} {} of beer.
{}, {} {} of beer on the wall.\n", if &now == "no more" {"No more"} else {&now}, bottle_now,
                                     now, bottle_now, store_or_drink, left, bottle_after}
}

pub fn sing(start: i32, end: i32) -> String {
    let mut buffer = String::with_capacity(((start - end) * 120) as usize); // 120 is aproximatedly the len of a verse
    for i in (end + 1..start + 1).rev() {
        buffer.push_str(&verse(i));
        buffer.push('\n');
    }
    buffer.push_str(&verse(end));
    buffer
}
