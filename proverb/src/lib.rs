pub fn build_proverb(list: &[&str]) -> String {
    if list.len() == 0 {
        return String::new();
    }
    let mut buffer = Vec::with_capacity(list.len());
    for i in 0..list.len() - 1 {
        buffer.push(format! {"For want of a {} the {} was lost.", list[i], list[i+1]});
    }
    buffer.push(format! {"And all for the want of a {}.", list.first().unwrap()});
    buffer.join("\n")
}
