pub fn reply(message: &str) -> &str {
    let message = message.trim();

    let alphabetic_chars: Vec<char> = message.chars().filter(|c| c.is_alphabetic()).collect();
    let is_shout = alphabetic_chars.len() > 0 && alphabetic_chars.iter().all(|c| c.is_uppercase());
    let is_question = message.ends_with("?");
    let is_empty = message.len() == 0 || message.chars().all(|c| c.is_whitespace());

    if is_empty {
        "Fine. Be that way!"
    } else if is_shout && is_question {
        "Calm down, I know what I'm doing!"
    } else if is_shout {
        "Whoa, chill out!"
    } else if is_question {
        "Sure."
    } else {
        "Whatever."
    }
}
