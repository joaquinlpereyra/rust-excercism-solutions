extern crate unicode_segmentation;
use std::string::String;
use unicode_segmentation::UnicodeSegmentation;

pub fn reverse(input: &str) -> String {
    UnicodeSegmentation::graphemes(input, true)
        .rev()
        .collect::<String>()
}
