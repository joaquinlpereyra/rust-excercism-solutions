use std::vec::Vec;

struct Stack<T> {
    vector: Vec<T>,
}

impl<T> Stack<T> {
    fn new(size: usize) -> Stack<T> {
        Stack {
            vector: Vec::with_capacity(size),
        }
    }

    fn push(&mut self, e: T) {
        self.vector.push(e)
    }

    fn pop(&mut self) -> Option<T> {
        self.vector.pop()
    }

    fn is_empty(&self) -> bool {
        self.vector.is_empty()
    }
}

pub fn brackets_are_balanced(string: &str) -> bool {
    let mut stack: Stack<char> = Stack::new(string.len());
    for c in string.chars() {
        if is_opening(c) {
            stack.push(c);
        } else if is_closing(c) {
            if let Some(chr) = stack.pop() {
                if opposite(c) != chr {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
    stack.is_empty()
}

fn is_opening(chr: char) -> bool {
    chr == '[' || chr == '{' || chr == '('
}

fn is_closing(chr: char) -> bool {
    chr == ']' || chr == '}' || chr == ')'
}

fn opposite(chr: char) -> char {
    match chr {
        ']' => '[',
        ')' => '(',
        '}' => '{',
        _ => ' ',
    }
}
