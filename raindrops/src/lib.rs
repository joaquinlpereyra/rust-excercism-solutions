use std::string::String;

pub fn raindrops(n: u32) -> String {
    let mut string = String::new();
    let map = [(3, "Pling"), (5, "Plang"), (7, "Plong")];
    for (factor, s) in &map {
        if n % factor == 0 {
            string.push_str(s);
        }
    }
    match string.as_ref() {
        "" => n.to_string(),
        _ => string,
    }
}
